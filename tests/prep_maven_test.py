import pytest
from cjp.export_prep import maven


@pytest.mark.parametrize(
    "input_string, pythonic_string",
    [
        (" Brad Case", "brad_case"),
        ("ConstituentLUID", "constituent_luid"),
        ("Cnst LUID", "cnst_luid"),
        ("Merchant acct.", "merchant_acct"),
    ],
)
def test_pythonic_string(input_string, pythonic_string):
    assert maven.make_str_pythonic(input_string) == pythonic_string


def test_pythonic_string_typecheck():
    with pytest.raises(TypeError):
        maven.make_str_pythonic(23)
