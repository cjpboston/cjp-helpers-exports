#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_namespace_packages

# with open('README.rst') as readme_file:
#     readme = readme_file.read()

requirements = ["pandas"]

setup(
    author="cjp",
    author_email="danylok@cjp.org",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
    ],
    description="A project to contain CJP's of the occasionally useful helper functions.",
    install_requires=requirements,
    license="BSD license",
    include_package_data=True,
    name="cjp_helpers_exports",
    packages=find_namespace_packages(),
    url="https://gitlab.com/cjpboston/cjp-helpers-exports",
    version="0.1.1",
    zip_safe=False,
)
