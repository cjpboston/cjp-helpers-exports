import re


def pythonic_df_columns(df, inplace=True):
    """Renames dataframe column names to be more pythonic. Edits in place or returns list of columns. 
    Translates "Great ColumnName" to "great_column_name"
    This allows for dot-column selection & autocomplete, i.e. df.great_column_name
    """
    cols = [make_str_pythonic(col) for col in df.columns]
    if len(set(cols)) != len(cols):
        raise ValueError("Reduced column names not unique.")
    if inplace:
        df.columns = cols  # edits in place
    else:
        return cols


def make_str_pythonic(string: str) -> str:
    """Converts strings to look pythonic. 
    Translates "Great ColumnName" to "great_column_name"
    """
    if not isinstance(string, str):
        raise TypeError("Expected string.")
    string = string.strip()
    string = re.sub(r"(?<!^)(?<![A-Z])([A-Z])", r"_\1", string)
    string = string.lower()
    string = re.sub(r"(\W+|_)+", "_", string) # convert multiple underscores or word breaks to spaces.
    string = string.strip('_') # remove edge underscores. 
    return string


def string_amount_to_float(column):
    """Given a pandas str series with amts in format $##,###.## returns as float.
    Used for fixing maven exports. 
    """
    # column = column.str.replace(",", "")
    # column = column.str.replace("$", "").astype("float")
    column = column.replace("\$|,", "", regex=True).astype(float)
    return column


def fix_bool(column, default: bool = False):
    """Given a pandas str col with bools in yes/no format returns bool columns.
    Used for fixing maven exports."""
    col = column.copy()
    del column
    col.loc[col == "No"] = False
    col.loc[col == "Yes"] = True
    col = col.fillna(default)
    return col


# def utc_to_est(d):
#     """d: Timestamp object, no timezone info.
#     Returns Timestamp object, no timezone info, offset to EST/EDT time."""
#     # alternate approach would be to store tzinfo.
#     if d is pd.NaT:
#         return d
#     EASTERN = timezone('US/Eastern') # 'EST' is not daylight-st aware.
#     try:
#         d = d.tz_localize(EASTERN)
#     except (NonExistentTimeError, AmbiguousTimeError): # solves the EST->EDT timeskip problem.
#         d = d + pd.Timedelta(hours=1)
#         d = d.tz_localize(EASTERN)
#     d = d + d.utcoffset()
#     d = d.replace(tzinfo=None)
#     return d

# def parse_datetimes(df, datetime_cols):
#     """Assumes UTC, converts to EST, nulls future and 0 dates."""
#     boundary_of_future = pd.datetime.now() + pd.DateOffset(days=1) # future starts tomorrow.
#     for col in datetime_cols:
#         if df[col].dtype == 'int64' or df[col].dtype == 'float64':
#             df[col] = pd.to_datetime(df[col], unit='ms') # ms if unix time
#         else:
#             df[col] = pd.to_datetime(df[col]) # otherwise take a guess
#         df.loc[(df[col] == '1970-01-01 00:00:00'), col] = pd.NaT # NaT unopened emails
#         df[col] = df[col].apply(lambda x: utc_to_est(x)) # VERY SLOW! Look for better way.
#         df.loc[(df[col] > boundary_of_future), col] = pd.NaT # NaT future dates (happens when email is opened 2+ times)

# def convert_date_to_campaign_year(datetime_col):
#     """Given a pandas datetime column, returns the associated cjp annual campaign year"""
#     cmp_year_offset = pd.DateOffset(months=5) # CJP campaign runs from August 1 to August 1.
#     return pd.DatetimeIndex(datetime_col + cmp_year_offset).year


# def fix_year(column):
#     """Given a pandas str col with years in format #,### returns as int.
#     Used for fixing maven exports. """
#     return column.str.replace(',','').astype('int')


# def nullify_weird_dates(df, datetime_cols):
#     """nullify future dates and 0 dates. Future starts 24 hours from now()"""
#     boundary_of_future = datetime.datetime.now() + datetime.timedelta(days=1)
#     for col in datetime_cols:
#         df.loc[(df[col] == '1970-01-01 00:00:00'), col] = pd.NaT # NaT unopened emails
#         df.loc[(df[col] > boundary_of_future), col] = pd.NaT # NaT future dates (happens when email is opened 2+ times)
